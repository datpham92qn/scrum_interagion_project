﻿namespace Ho_Tro_Nau_An
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.trangChủToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhSáchMónĂnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hỗTrợNấuĂnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mónĂnSángToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mónĂnTrưaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mónĂnTốiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kiểmTraDinhDưỡngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.muaHàngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hỗTrợToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýMónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýTàiKhoảnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tìmKiếmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pnlName = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.pnlName.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(69, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 28);
            this.label1.TabIndex = 38;
            this.label1.Text = "Hỗ Trợ Nấu Ăn";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(5, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(73, 34);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Location = new System.Drawing.Point(5, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1275, 598);
            this.panel1.TabIndex = 41;
            // 
            // trangChủToolStripMenuItem
            // 
            this.trangChủToolStripMenuItem.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trangChủToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("trangChủToolStripMenuItem.Image")));
            this.trangChủToolStripMenuItem.Name = "trangChủToolStripMenuItem";
            this.trangChủToolStripMenuItem.Size = new System.Drawing.Size(120, 27);
            this.trangChủToolStripMenuItem.Text = "Trang Chủ";
            // 
            // danhSáchMónĂnToolStripMenuItem
            // 
            this.danhSáchMónĂnToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("danhSáchMónĂnToolStripMenuItem.Image")));
            this.danhSáchMónĂnToolStripMenuItem.Name = "danhSáchMónĂnToolStripMenuItem";
            this.danhSáchMónĂnToolStripMenuItem.Size = new System.Drawing.Size(205, 27);
            this.danhSáchMónĂnToolStripMenuItem.Text = "Danh Sách Món Ăn";
            this.danhSáchMónĂnToolStripMenuItem.Click += new System.EventHandler(this.danhSáchMónĂnToolStripMenuItem_Click);
            // 
            // hỗTrợNấuĂnToolStripMenuItem
            // 
            this.hỗTrợNấuĂnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mónĂnSángToolStripMenuItem,
            this.mónĂnTrưaToolStripMenuItem,
            this.mónĂnTốiToolStripMenuItem});
            this.hỗTrợNấuĂnToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("hỗTrợNấuĂnToolStripMenuItem.Image")));
            this.hỗTrợNấuĂnToolStripMenuItem.Name = "hỗTrợNấuĂnToolStripMenuItem";
            this.hỗTrợNấuĂnToolStripMenuItem.Size = new System.Drawing.Size(168, 27);
            this.hỗTrợNấuĂnToolStripMenuItem.Text = "Hỗ Trợ Nấu Ăn";
            // 
            // mónĂnSángToolStripMenuItem
            // 
            this.mónĂnSángToolStripMenuItem.Name = "mónĂnSángToolStripMenuItem";
            this.mónĂnSángToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.mónĂnSángToolStripMenuItem.Text = "Món Ăn Sáng";
            // 
            // mónĂnTrưaToolStripMenuItem
            // 
            this.mónĂnTrưaToolStripMenuItem.Name = "mónĂnTrưaToolStripMenuItem";
            this.mónĂnTrưaToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.mónĂnTrưaToolStripMenuItem.Text = "Món Ăn Trưa";
            // 
            // mónĂnTốiToolStripMenuItem
            // 
            this.mónĂnTốiToolStripMenuItem.Name = "mónĂnTốiToolStripMenuItem";
            this.mónĂnTốiToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.mónĂnTốiToolStripMenuItem.Text = "Món Ăn Tối";
            // 
            // kiểmTraDinhDưỡngToolStripMenuItem
            // 
            this.kiểmTraDinhDưỡngToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("kiểmTraDinhDưỡngToolStripMenuItem.Image")));
            this.kiểmTraDinhDưỡngToolStripMenuItem.Name = "kiểmTraDinhDưỡngToolStripMenuItem";
            this.kiểmTraDinhDưỡngToolStripMenuItem.Size = new System.Drawing.Size(227, 27);
            this.kiểmTraDinhDưỡngToolStripMenuItem.Text = "Kiểm Tra Dinh Dưỡng";
            // 
            // muaHàngToolStripMenuItem
            // 
            this.muaHàngToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("muaHàngToolStripMenuItem.Image")));
            this.muaHàngToolStripMenuItem.Name = "muaHàngToolStripMenuItem";
            this.muaHàngToolStripMenuItem.Size = new System.Drawing.Size(130, 27);
            this.muaHàngToolStripMenuItem.Text = "Mua Hàng";
            // 
            // hỗTrợToolStripMenuItem
            // 
            this.hỗTrợToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("hỗTrợToolStripMenuItem.Image")));
            this.hỗTrợToolStripMenuItem.Name = "hỗTrợToolStripMenuItem";
            this.hỗTrợToolStripMenuItem.Size = new System.Drawing.Size(101, 27);
            this.hỗTrợToolStripMenuItem.Text = "Hỗ Trợ";
            // 
            // quảnLýMónToolStripMenuItem
            // 
            this.quảnLýMónToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("quảnLýMónToolStripMenuItem.Image")));
            this.quảnLýMónToolStripMenuItem.Name = "quảnLýMónToolStripMenuItem";
            this.quảnLýMónToolStripMenuItem.Size = new System.Drawing.Size(156, 27);
            this.quảnLýMónToolStripMenuItem.Text = "Quản Lý Món";
            // 
            // quảnLýTàiKhoảnToolStripMenuItem
            // 
            this.quảnLýTàiKhoảnToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("quảnLýTàiKhoảnToolStripMenuItem.Image")));
            this.quảnLýTàiKhoảnToolStripMenuItem.Name = "quảnLýTàiKhoảnToolStripMenuItem";
            this.quảnLýTàiKhoảnToolStripMenuItem.Size = new System.Drawing.Size(204, 27);
            this.quảnLýTàiKhoảnToolStripMenuItem.Text = "Quản Lý Tài Khoản";
            // 
            // tìmKiếmToolStripMenuItem
            // 
            this.tìmKiếmToolStripMenuItem.Image = global::Ho_Tro_Nau_An.Properties.Resources.bell;
            this.tìmKiếmToolStripMenuItem.Name = "tìmKiếmToolStripMenuItem";
            this.tìmKiếmToolStripMenuItem.Size = new System.Drawing.Size(123, 27);
            this.tìmKiếmToolStripMenuItem.Text = "Tìm Kiếm";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 27);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trangChủToolStripMenuItem,
            this.danhSáchMónĂnToolStripMenuItem,
            this.hỗTrợNấuĂnToolStripMenuItem,
            this.kiểmTraDinhDưỡngToolStripMenuItem,
            this.muaHàngToolStripMenuItem,
            this.hỗTrợToolStripMenuItem,
            this.quảnLýMónToolStripMenuItem,
            this.quảnLýTàiKhoảnToolStripMenuItem,
            this.tìmKiếmToolStripMenuItem,
            this.toolStripTextBox1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1544, 31);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // pnlName
            // 
            this.pnlName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pnlName.Controls.Add(this.label1);
            this.pnlName.Controls.Add(this.pictureBox1);
            this.pnlName.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlName.Location = new System.Drawing.Point(0, 0);
            this.pnlName.Margin = new System.Windows.Forms.Padding(4);
            this.pnlName.Name = "pnlName";
            this.pnlName.Size = new System.Drawing.Size(1280, 34);
            this.pnlName.TabIndex = 40;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Location = new System.Drawing.Point(36, 58);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(750, 419);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1280, 635);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.IsMdiContainer = true;
            this.Name = "Home";
            this.Text = "Trang Chủ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlName.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem trangChủToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danhSáchMónĂnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hỗTrợNấuĂnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mónĂnSángToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mónĂnTrưaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mónĂnTốiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kiểmTraDinhDưỡngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem muaHàngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hỗTrợToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýMónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýTàiKhoảnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tìmKiếmToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlName;
    }
}

