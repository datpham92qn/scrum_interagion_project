﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Ho_Tro_Nau_An.UI
{
    public partial class Danh_Sach_mon : UserControl
    {
        Database.DBDataContext DB = new Database.DBDataContext();
        public Danh_Sach_mon()
        {
            InitializeComponent();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            listView2.Items.Clear();
            string depid = e.Node.Tag.ToString();
            var listdanhsachmon = from emp in DB.Danh_Sach_Mons
                                  where emp.Id_danhmuc == depid
                                  select new { emp.Id,emp.Ten_Mon, emp.Chitiet, emp.Thoigian,  emp.Luuy };
            foreach (var emp in listdanhsachmon)
            {
                ListViewItem item = new ListViewItem(emp.Id);
                item.SubItems.Add(emp.Ten_Mon);
                item.SubItems.Add(emp.Chitiet);
                item.SubItems.Add(emp.Thoigian);
                item.SubItems.Add(emp.Luuy);

                listView2.Items.Add(item);
            }
        }

        private void listView2_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedItems.Count > 0)
            {
                ListViewItem item = listView2.SelectedItems[0];
                MessageBox.Show(item.Text + ":" + item.SubItems[1] + ":" + item.SubItems[2].Text);

            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Danh_Sach_mon_Load(object sender, EventArgs e)
        {
            TreeNode root = new TreeNode("Danh Sách Món Ăn", 0, 0);
            root.Tag = 0;
            foreach (var dep in DB.Danh_mucs)
            {
                TreeNode chidl = new TreeNode(dep.Ten_danh_muc, 1, 1);
                chidl.Tag = dep.Id;
                root.Nodes.Add(chidl);
            }
            treeView1.Nodes.Add(root);
            treeView1.ExpandAll();
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            listView2.View = View.Details;
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            listView2.View = View.LargeIcon;
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            listView2.View = View.List;
        }

        private void radioButton9_CheckedChanged(object sender, EventArgs e)
        {
            listView2.View = View.Tile;
        }

        private void radioButton10_CheckedChanged(object sender, EventArgs e)
        {
            listView2.View = View.SmallIcon;
        }
    }
}
